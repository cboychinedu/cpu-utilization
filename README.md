<h1> CPU UTILIZATION ANALYSIS </h1>

## Description
<img src="doc_files/cpu_utils.jpg" alt="cpu_utils" width="900" height="500"> <br>

<p>
This is a python script that extracts the <b> CPU UTILIZATION </b> values, and stores them in a <b> MySQL </b> database server.  
</p>


## Dependencies

* For this program to function, the required dependencies are needed. 
* mysql.connector 
* psutil 
* schedule 

## Installing

* Clone the repository into your local working directory, open a command line interface terminal on <b> Linux </b> or cmd on <b> Windows </b>, and type the command below. 
 
```
$ pip install -r requirements.txt 

```

## Program Execution 

### Create A Database 
* To create a database, edit the <b> create_db.py </b> file, and make changes to the <br> 
<b> host </b> <br> 
<b> user </b> <br> 
<b> password </b> variables. <br>

After editing and saving, open a command line interface terminal on <b> Linux </b> or cmd on <b> Windows </b>, and type the command below to <b> create </b> the database inside the <b> MySQL </b> server. 


```
$ python create_db.py 

```

### Create A Table 
* To create a table, edit the <b> create_table.py </b> file, and make changes to the <br>
<b> host </b> <br> 
<b> user </b> <br> 
<b> password </b> <br>
<b> database </b>  variables. <br>

After editing and saving, type the command below on a command line interface to <b> create </b> the table inside the <b> MySQL </b> database. 


```
$ python create_table.py

```

### Delete A Table 
* To delete a table, edit the <b> delete_table.py </b> file, and make changes to the <br> 
<b> host </b> <br>
<b> user </b> <br>
<b> password </b> <br>
<b> database </b> variable. <br>

After editing and saving, type the command below on a command line interface to <b> delete </b> the table inside 
the <b> MySQL </b> database. 


```
$ python delete_table.py 

```

### Insert Values Into A Database 
* To insert <b> CPU UTILIZATION </b> values into the database, the helper script <b> insert_to_db.py </b> file perfoms this 
functionality. <br>
But the main features was included into the <b> main.py </b> file as a python class <b> method </b> within the main class called <b> MySQL_function </b>. <br> 

To manually insert datas or values into the database, without executing the <b> main.py </b> python script file, open a command line interface terminal on <b> Linux </b> or a cmd on <b> Windows </b>, and type the following command below. 


```
$ python insert_to_db.py 

```

### Convert Database Values Into CSV Format 
* To convert <b> CPU UTILIZATION </b> values into csv format, the script <b> convert_to_csv.py </b> is responsible for this functionality. <br>
it's function was also included into the <b> main.py </b> script as a method within the class <b> sql_classes </b>. But for manually converting data to 
csv from the command line interface, execute the script in the code block section below. 

```
$ python convert_to_csv.py 

``` 


### Execute The Main Script
* To execute the program, open a command line interface terminal on <b> Linux </b> or cmd on <b> Windows </b>, and type the command below. 

```
$ python main.py 

```

### Help

### Authors

### Version History

### License

### Acknowledgments

