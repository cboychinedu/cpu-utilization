#!/usr/bin/env python3 

# DESCRIPTION
"""
CPU usage or utilization refers to the time taken by a computer to process some 
information. RAM usage or MAIN MEMORY UTILIZATION on the other hand refers to 
the amount of time RAM is used by a certain system at a particular time. 
Both of these can be retrieved using python. 
"""

# Importing the necessary modules
import platform
import time 
import schedule 
import psutil as ps 
from datetime import datetime as dt
from modules.mysql_classes import MySQL_function


# Creating an instance of the MySQL_function class for saving into the database 
mysql_class = MySQL_function()


# Creating a function for getting the actual size in bytes formats 
def get_size(bytes, suffix="B"):
    # Setting the factor as "1024"
    factor = 1024 

    # Loop through the array and check if the bytes 
    # values are less than the factor specified above 
    for unit in ["", "K", "M", "G", "T", "P"]: 
        if bytes < factor: 
            # If the bytes are less than the factor, execute 
            # the block of code below 
            return f"{bytes:.2f}{unit}{suffix}"
        
        # If the bytes are greater than the factor, execute the code 
        # block below. 
        bytes /= factor 


# System information 
uname = platform.uname() 
SYSTEM_NAME = uname.system 
NODE_NAME = uname.node 
SYSTEM_RELEASE = uname.release 
SYSTEM_VERSION = uname.version 
MACHINE = uname.machine 
SYSTEM_PROCESSOR = uname.processor 

# Getting the CPU swap memory 
swap = ps.swap_memory() 

# Getting the CPU Disk Usage information 
partitions = ps.disk_partitions() 

# Getting the CPU UTILIZATION properties
CPU_CORE_TEMP = ps.sensors_temperatures()["coretemp"][0][1] 
CPU_BOOT_TIME = ps.boot_time() 
CPU_COUNT = ps.cpu_count() 
CPU_CTX_SWITCHES = ps.cpu_stats()[0]
CPU_CTX_INTERRUPTS = ps.cpu_stats()[1]
CPU_CTX_SOFT_INTERRUPTS = ps.cpu_stats()[2]
CPU_PERCENT = ps.cpu_percent()
CPU_BATTERY = ps.sensors_battery()[0]
CPU_BATTERY_PLUGED = ps.sensors_battery()[2]
CPU_FREQUENCY = ps.cpu_freq()[0]
CPU_FREQUENCY_MAX = ps.cpu_freq()[2]
CPU_FREQUENCY_MIN = ps.cpu_freq()[1]
CPU_TOTAL_MEMORY = ps.virtual_memory()[0]
CPU_AVAILABLE_MEMORY = ps.virtual_memory()[1]
CPU_MEMORY_PERCENT = ps.virtual_memory()[2]
CPU_USED_MEMORY = ps.virtual_memory()[3] 
CPU_FREE_MEMORY = ps.virtual_memory()[4]
CPU_ACTIVE_MEMORY = ps.virtual_memory()[5]
CPU_INACTIVE_MEMORY = ps.virtual_memory()[6]
CPU_SHARED_MEMORY = ps.virtual_memory()[9]
CPU_TOTAL_SWAP_MEMORY = get_size(swap.total)
CPU_SWAP_FREE_MEMORY = get_size(swap.free)
CPU_SWAP_USED_MEMORY = get_size(swap.used)
CPU_SWAP_MEMORY_PERCENT = get_size(swap.percent) 

# Getting the disk partitions
for partition in partitions: 
    # Getting all the partition devices 
    CPU_DEVICE_PARTITION = partition.device 
    CPU_MOUNT_POINT = partition.mountpoint 
    CPU_FILE_SYSTEM = partition.fstype 

    # Using a try, except block to check the mount point
    try:
        # Get the partition usage for the mount point 
        partition_usage = ps.disk_usage(partition.mountpoint) 
    
    # On permission error, continue the for-loop process. 
    except PermissionError: 
        # Continue the program execution on "PermissionError" 
        continue; 
    
    # Get all the partition usage statics, including the total disk size,
    # the used memory, free memory, and the memory usage percentage. 
    CPU_PARTITION_TOTAL_SIZE = get_size(partition_usage.total) 
    CPU_PARTITION_USED_SIZE = get_size(partition_usage.used)
    CPU_PARTITION_FREE_SIZE = get_size(partition_usage.free) 
    CPU_PARTITION_PERCENTAGE = get_size(partition_usage.percent)

# Get the IO statistics since boot time 
disk_io = ps.disk_io_counters() 
CPU_IO_DISK_SIZE = get_size(disk_io.read_bytes) 
CPU_IO_TOTAL_WRITE_SIZE = get_size(disk_io.write_bytes)

# Setting the datetime for every data collection of 
# the CPU utilization properties
# Firstly, get the actual date values as a tuple, then 
# Convert the tuple date values into a string type explictly. 
today_date = dt.now() 
DATE_VALUE = dt.isoformat(today_date) 


# Schedule for the specified time interval.
# Setting the time interval for 10 seconds. 
time_interval = 10; 

# Creating a function for inserting the values into the database at 3 seconds intervals 
def update_db():
    # Insert values into the database. 
    mysql_class.insert_to_db(
        DATE_VALUE, SYSTEM_NAME, NODE_NAME, SYSTEM_RELEASE, SYSTEM_VERSION, MACHINE, SYSTEM_PROCESSOR, CPU_CORE_TEMP, 
        CPU_BOOT_TIME, CPU_COUNT, CPU_CTX_SWITCHES, CPU_CTX_INTERRUPTS, CPU_CTX_SOFT_INTERRUPTS, CPU_PERCENT, CPU_BATTERY, CPU_BATTERY_PLUGED, 
        CPU_FREQUENCY, CPU_FREQUENCY_MAX, CPU_FREQUENCY_MIN, CPU_TOTAL_MEMORY, CPU_AVAILABLE_MEMORY, CPU_MEMORY_PERCENT, CPU_USED_MEMORY, 
        CPU_FREE_MEMORY, CPU_ACTIVE_MEMORY, CPU_INACTIVE_MEMORY, CPU_SHARED_MEMORY, CPU_TOTAL_SWAP_MEMORY, CPU_SWAP_FREE_MEMORY, 
        CPU_SWAP_USED_MEMORY, CPU_SWAP_MEMORY_PERCENT, CPU_DEVICE_PARTITION, CPU_MOUNT_POINT, CPU_FILE_SYSTEM, CPU_PARTITION_TOTAL_SIZE,
        CPU_PARTITION_USED_SIZE, CPU_PARTITION_FREE_SIZE, CPU_PARTITION_PERCENTAGE, CPU_IO_DISK_SIZE, CPU_IO_TOTAL_WRITE_SIZE
    )

    # Saving the newly updated files into a csv file, and dump
    # the csv file to the current working directory
    mysql_class.save_to_csv()


# Setting the schedule configuration settings 
schedule.every(time_interval).seconds.do(update_db)

# Setting the conditions for the while loop 
running = True 

# Setting the while loop 
while running:
    # Execute the block of code below when this script is
    # executed.
    print('[INFO] Database Updated with CPU Utilization Values ...')
    schedule.run_pending()

    # Sleeping for 5 seconds
    time.sleep(5)



# rgb(37, 37, 38) none repeat scroll 0% 0%

