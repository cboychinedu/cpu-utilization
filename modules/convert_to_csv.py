#!/usr/bin/env python3 

# Imporing the necessary modules 
import csv
import mysql.connector 

# Making a connection to the database "mydb" 
mydb = mysql.connector.connect(
    host = "localhost", 
    user = "root", 
    password = "mysql4321@", 
    database = "mydb"
)

# Creating the cursor object 
mycursor = mydb.cursor() 

# Creating the SQL statement 
sql_statement = """
    select * from mydb.cpu_utils; 
"""

# Execute the sql statement 
mycursor.execute(sql_statement) 

# Fetch all the data from the database, and save 
# them into the variable called "data" 
data = mycursor.fetchall() 

# Creating the header for the csv file 
header = [ "DATE_VALUE", "SYSTEM_NAME", "NODE_NAME", "SYSTEM_RELEASE", "SYSTEM_VERSION", "MACHINE", "SYSTEM_PROCESSOR", "CPU_CORE_TEMP", "CPU_BOOT_TIME",
        "CPU_COUNT", "CPU_CTX_SWITCHES", "CPU_CTX_INTERRUPTS", "CPU_CTX_SOFT_INTERRUPTS", "CPU_PERCENT", "CPU_BATTERY", "CPU_BATTERY_PLUGED", 
        "CPU_FREQUENCY", "CPU_FREQUENCY_MAX", "CPU_FREQUENCY_MIN", "CPU_TOTAL_MEMORY", "CPU_AVAILABLE_MEMORY", "CPU_MEMORY_PERCENT", "CPU_USED_MEMORY", 
        "CPU_FREE_MEMORY", "CPU_ACTIVE_MEMORY", "CPU_INACTIVE_MEMORY", "CPU_SHARED_MEMORY", "CPU_TOTAL_SWAP_MEMORY", "CPU_SWAP_FREE_MEMORY", 
        "CPU_SWAP_USED_MEMORY", "CPU_SWAP_MEMORY_PERCENT", "CPU_DEVICE_PARTITION", "CPU_MOUNT_POINT", "CPU_FILE_SYSTEM", "CPU_PARTITION_TOTAL_SIZE",
        "CPU_PARTITION_USED_SIZE", "CPU_PARTITION_FREE_SIZE", "CPU_PARTITION_PERCENTAGE", "CPU_IO_DISK_SIZE", "CPU_IO_TOTAL_WRITE_SIZE" 
    ]

# Convert the saved data in memory into a csv file, 
# and save it to disk. 
with open("db_data.csv", 'w', encoding="UTF-8", newline="") as f:
    # Create the writer cursor object
    writer = csv.writer(f) 

    # Write both the header, and the database data into 
    # the csv file, and save to disk 
    writer.writerow(header)
    writer.writerows(data) 