#!/usr/bin/env python3 

# Importing the necessary modules 
import mysql.connector 

# Making a connection to the database "mydb" 
mydb = mysql.connector.connect(
    host = "localhost", 
    user = "root", 
    password = "mysql4321@", 
    database = "mydb"
)

# Creating the cursor object 
mycursor = mydb.cursor() 

# Creating an SQL statement 
sql_statement = """
    CREATE TABLE cpu_utils (id INT AUTO_INCREMENT PRIMARY KEY, 
    DATE_VALUE VARCHAR(255), SYSTEM_NAME VARCHAR(255), NODE_NAME VARCHAR(255), SYSTEM_RELEASE VARCHAR(255), 
    SYSTEM_VERSION VARCHAR(255), MACHINE VARCHAR(255), SYSTEM_PROCESSOR VARCHAR(255), CPU_CORE_TEMP VARCHAR(255), 
    CPU_BOOT_TIME VARCHAR(255), CPU_COUNT VARCHAR(255), CPU_CTX_SWITCHES VARCHAR(255), CPU_CTX_INTERRUPTS VARCHAR(255), 
    CPU_CTX_SOFT_INTERRUPTS VARCHAR(255), CPU_PERCENT VARCHAR(255), CPU_BATTERY VARCHAR(255), CPU_BATTERY_PLUGED VARCHAR(255), 
    CPU_FREQUENCY VARCHAR(255), CPU_FREQUENCY_MAX VARCHAR(255), CPU_FREQUENCY_MIN VARCHAR(255), CPU_TOTAL_MEMORY VARCHAR(255), 
    CPU_AVAILABLE_MEMORY VARCHAR(255), CPU_MEMORY_PERCENT VARCHAR(255), CPU_USED_MEMORY VARCHAR(255), CPU_FREE_MEMORY VARCHAR(255), 
    CPU_ACTIVE_MEMORY VARCHAR(255), CPU_INACTIVE_MEMORY VARCHAR(255), CPU_SHARED_MEMORY VARCHAR(255), CPU_TOTAL_SWAP_MEMORY VARCHAR(255), 
    CPU_SWAP_FREE_MEMORY VARCHAR(255), CPU_SWAP_USED_MEMORY VARCHAR(255), CPU_SWAP_MEMORY_PERCENT VARCHAR(255), CPU_DEVICE_PARTITION VARCHAR(50), 
    CPU_MOUNT_POINT VARCHAR(50), CPU_FILE_SYSTEM VARCHAR(50), CPU_PARTITION_TOTAL_SIZE VARCHAR(255), CPU_PARTITION_USED_SIZE VARCHAR(255), CPU_PARTITION_FREE_SIZE VARCHAR(255), 
    CPU_PARTITION_PERCENTAGE VARCHAR(255), CPU_IO_DISK_SIZE VARCHAR(255), CPU_IO_TOTAL_WRITE_SIZE VARCHAR(255)    
)"""; 

# Execute the sql statement for creating a table 
mycursor.execute(sql_statement) 