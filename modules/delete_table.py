#!/usr/bin/env python3 

# Importing the necessary modules 
import mysql.connector 

# Making a connection to the database "mydb" 
mydb = mysql.connector.connect(
    host = "localhost", 
    user = "root", 
    password = "mysql4321@", 
    database = "mydb"
)

# Creating the cursor object 
mycursor = mydb.cursor() 

# Sql statement 
sql_statement = "DROP TABLE cpu_utils"

# Execute sql statement 
mycursor.execute(sql_statement)