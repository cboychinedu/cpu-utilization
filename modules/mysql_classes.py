#!/usr/bin/env python3 

# Importing the necessary modules 
import csv
import mysql.connector

# Making a connection to the databae "mydb" 
mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="mysql4321@",
    database="mydb"
)

# Creating the cursor object 
mycursor = mydb.cursor()


# Creating a class for performing the sql functions 
class MySQL_function:
    # Creating a blue print for the init method. 
    def __init__(self):
        # Creating the header for the csv file 
        self.header = ["DATE_VALUE", "SYSTEM_NAME", "NODE_NAME", "SYSTEM_RELEASE", "SYSTEM_VERSION", "MACHINE",
                       "SYSTEM_PROCESSOR", "CPU_CORE_TEMP", "CPU_BOOT_TIME",
                       "CPU_COUNT", "CPU_CTX_SWITCHES", "CPU_CTX_INTERRUPTS", "CPU_CTX_SOFT_INTERRUPTS", "CPU_PERCENT",
                       "CPU_BATTERY", "CPU_BATTERY_PLUGED",
                       "CPU_FREQUENCY", "CPU_FREQUENCY_MAX", "CPU_FREQUENCY_MIN", "CPU_TOTAL_MEMORY",
                       "CPU_AVAILABLE_MEMORY", "CPU_MEMORY_PERCENT", "CPU_USED_MEMORY",
                       "CPU_FREE_MEMORY", "CPU_ACTIVE_MEMORY", "CPU_INACTIVE_MEMORY", "CPU_SHARED_MEMORY",
                       "CPU_TOTAL_SWAP_MEMORY", "CPU_SWAP_FREE_MEMORY",
                       "CPU_SWAP_USED_MEMORY", "CPU_SWAP_MEMORY_PERCENT", "CPU_DEVICE_PARTITION", "CPU_MOUNT_POINT",
                       "CPU_FILE_SYSTEM", "CPU_PARTITION_TOTAL_SIZE",
                       "CPU_PARTITION_USED_SIZE", "CPU_PARTITION_FREE_SIZE", "CPU_PARTITION_PERCENTAGE",
                       "CPU_IO_DISK_SIZE", "CPU_IO_TOTAL_WRITE_SIZE"
                       ]

    # Creating a method for extracting all the values from the database, 
    # And save them on disk as a csv file 
    def save_to_csv(self):
        # Execute this block of code if the user want to extract all the datas from 
        # the database and save to disk as a csv file 
        # Creating the SQL statements 
        sql_statement = """
            SELECT * FROM mydb.cpu_utils; 
        """

        # Execute the sql statement 
        mycursor.execute(sql_statement)
        data = mycursor.fetchall()

        # Convert the saved data in memory into a csv file, 
        # and save it to disk 
        with open("csv/db_data.csv", "w", encoding="UTF-8", newline="") as f:
            # Creating the writer cursor object 
            writer = csv.writer(f)

            # Write both the header, and the database data into the 
            # csv file, and save it to disk. 
            writer.writerow(self.header)
            writer.writerows(data)

            # Creating a method for creating a database

    def create_database(self):
        # Execute the block of code below for creation of a database 
        mycursor.execute("CREATE DATABASE mydb")

    # Creating a method for creating an sql table
    def create_table(self):
        # Execute this block of code below 
        # Creating an SQL statement 
        sql_statement = """CREATE TABLE cpu_utils (id INT AUTO_INCREMENT PRIMARY KEY, DATE_VALUE VARCHAR(255), 
        SYSTEM_NAME VARCHAR(255), NODE_NAME VARCHAR(255), SYSTEM_RELEASE VARCHAR(255), SYSTEM_VERSION VARCHAR(255), 
        MACHINE VARCHAR(255), SYSTEM_PROCESSOR VARCHAR(255), CPU_CORE_TEMP VARCHAR(255), CPU_BOOT_TIME VARCHAR(255), 
        CPU_COUNT VARCHAR(255), CPU_CTX_SWITCHES VARCHAR(255), CPU_CTX_INTERRUPTS VARCHAR(255), 
        CPU_CTX_SOFT_INTERRUPTS VARCHAR(255), CPU_PERCENT VARCHAR(255), CPU_BATTERY VARCHAR(255), CPU_BATTERY_PLUGED 
        VARCHAR(255), CPU_FREQUENCY VARCHAR(255), CPU_FREQUENCY_MAX VARCHAR(255), CPU_FREQUENCY_MIN VARCHAR(255), 
        CPU_TOTAL_MEMORY VARCHAR(255), CPU_AVAILABLE_MEMORY VARCHAR(255), CPU_MEMORY_PERCENT VARCHAR(255), 
        CPU_USED_MEMORY VARCHAR(255), CPU_FREE_MEMORY VARCHAR(255), CPU_ACTIVE_MEMORY VARCHAR(255), 
        CPU_INACTIVE_MEMORY VARCHAR(255), CPU_SHARED_MEMORY VARCHAR(255), CPU_TOTAL_SWAP_MEMORY VARCHAR(255), 
        CPU_SWAP_FREE_MEMORY VARCHAR(255), CPU_SWAP_USED_MEMORY VARCHAR(255), CPU_SWAP_MEMORY_PERCENT VARCHAR(255), 
        CPU_DEVICE_PARTITION VARCHAR(50), CPU_MOUNT_POINT VARCHAR(50), CPU_FILE_SYSTEM VARCHAR(50), 
        CPU_PARTITION_TOTAL_SIZE VARCHAR(255), CPU_PARTITION_USED_SIZE VARCHAR(255), CPU_PARTITION_FREE_SIZE VARCHAR(
        255), CPU_PARTITION_PERCENTAGE VARCHAR(255), CPU_IO_DISK_SIZE VARCHAR(255), CPU_IO_TOTAL_WRITE_SIZE VARCHAR(
        255) )""";

        # Execute the sql statement for creating a table 
        mycursor.execute(sql_statement)

        # Creating a method for saving the sql values

    def insert_to_db(self, DATE_VALUE, SYSTEM_NAME, NODE_NAME, SYSTEM_RELEASE, SYSTEM_VERSION, MACHINE,
                     SYSTEM_PROCESSOR, CPU_CORE_TEMP,
                     CPU_BOOT_TIME, CPU_COUNT, CPU_CTX_SWITCHES, CPU_CTX_INTERRUPTS, CPU_CTX_SOFT_INTERRUPTS,
                     CPU_PERCENT, CPU_BATTERY, CPU_BATTERY_PLUGED,
                     CPU_FREQUENCY, CPU_FREQUENCY_MAX, CPU_FREQUENCY_MIN, CPU_TOTAL_MEMORY, CPU_AVAILABLE_MEMORY,
                     CPU_MEMORY_PERCENT, CPU_USED_MEMORY,
                     CPU_FREE_MEMORY, CPU_ACTIVE_MEMORY, CPU_INACTIVE_MEMORY, CPU_SHARED_MEMORY, CPU_TOTAL_SWAP_MEMORY,
                     CPU_SWAP_FREE_MEMORY,
                     CPU_SWAP_USED_MEMORY, CPU_SWAP_MEMORY_PERCENT, CPU_DEVICE_PARTITION, CPU_MOUNT_POINT,
                     CPU_FILE_SYSTEM, CPU_PARTITION_TOTAL_SIZE,
                     CPU_PARTITION_USED_SIZE, CPU_PARTITION_FREE_SIZE, CPU_PARTITION_PERCENTAGE, CPU_IO_DISK_SIZE,
                     CPU_IO_TOTAL_WRITE_SIZE):
        # Execute the block of code below 
        # Creating the SQL statement 
        sql_statement = """INSERT INTO cpu_utils (DATE_VALUE, SYSTEM_NAME, NODE_NAME, SYSTEM_RELEASE, SYSTEM_VERSION, 
        MACHINE, SYSTEM_PROCESSOR, CPU_CORE_TEMP, CPU_BOOT_TIME, CPU_COUNT, CPU_CTX_SWITCHES, CPU_CTX_INTERRUPTS, 
        CPU_CTX_SOFT_INTERRUPTS, CPU_PERCENT, CPU_BATTERY, CPU_BATTERY_PLUGED, CPU_FREQUENCY, CPU_FREQUENCY_MAX, 
        CPU_FREQUENCY_MIN, CPU_TOTAL_MEMORY, CPU_AVAILABLE_MEMORY, CPU_MEMORY_PERCENT, CPU_USED_MEMORY, 
        CPU_FREE_MEMORY, CPU_ACTIVE_MEMORY, CPU_INACTIVE_MEMORY, CPU_SHARED_MEMORY, CPU_TOTAL_SWAP_MEMORY, 
        CPU_SWAP_FREE_MEMORY, CPU_SWAP_USED_MEMORY, CPU_SWAP_MEMORY_PERCENT, CPU_DEVICE_PARTITION, CPU_MOUNT_POINT, 
        CPU_FILE_SYSTEM, CPU_PARTITION_TOTAL_SIZE, CPU_PARTITION_USED_SIZE, CPU_PARTITION_FREE_SIZE, 
        CPU_PARTITION_PERCENTAGE, CPU_IO_DISK_SIZE, CPU_IO_TOTAL_WRITE_SIZE) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, 
        %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, 
        %s, %s, %s, %s, %s) 

        """

        # Getting all the values needed for cpu utilization
        values = (
        DATE_VALUE, SYSTEM_NAME, NODE_NAME, SYSTEM_RELEASE, SYSTEM_VERSION, MACHINE, SYSTEM_PROCESSOR, CPU_CORE_TEMP,
        CPU_BOOT_TIME, CPU_COUNT, CPU_CTX_SWITCHES, CPU_CTX_INTERRUPTS, CPU_CTX_SOFT_INTERRUPTS, CPU_PERCENT,
        CPU_BATTERY, CPU_BATTERY_PLUGED,
        CPU_FREQUENCY, CPU_FREQUENCY_MAX, CPU_FREQUENCY_MIN, CPU_TOTAL_MEMORY, CPU_AVAILABLE_MEMORY, CPU_MEMORY_PERCENT,
        CPU_USED_MEMORY,
        CPU_FREE_MEMORY, CPU_ACTIVE_MEMORY, CPU_INACTIVE_MEMORY, CPU_SHARED_MEMORY, CPU_TOTAL_SWAP_MEMORY,
        CPU_SWAP_FREE_MEMORY,
        CPU_SWAP_USED_MEMORY, CPU_SWAP_MEMORY_PERCENT, CPU_DEVICE_PARTITION, CPU_MOUNT_POINT, CPU_FILE_SYSTEM,
        CPU_PARTITION_TOTAL_SIZE,
        CPU_PARTITION_USED_SIZE, CPU_PARTITION_FREE_SIZE, CPU_PARTITION_PERCENTAGE, CPU_IO_DISK_SIZE,
        CPU_IO_TOTAL_WRITE_SIZE
        )

        # Executing the sql code 
        mycursor.execute(sql_statement, values)

        # Commiting 
        mydb.commit()
